//
// Base FM file for March 11
//
// 3 Voices:
// - Each voice is double carrier/double modulator
// - Single ratio for a voice
// - Oscillator spread will push one carrier/modulator pair away from the other
//
// Controls over OSC:
// - notes array contents
// - velocities of notes as separate array
// - durations of notes as separate array
// - degree of randomization with phrase arrays
// - ability to play directly to a voice
// - pan position of each voice
// - ratio
// - oscillator spread
// - modulation depth (treat as one per voice)
//

Mix2 mix => dac;
// Voice 1
SinOsc c[2] => ADSR ac[2];
SinOsc m[2] => ADSR am[2] => c;

ac[0] => Gain gL => mix.left;
ac[1] => Gain gR => mix.right;

// Voice 2
SinOsc c2[2] => ADSR ac2[2];
SinOsc m2[2] => ADSR am2[2] => c2;

ac2[0] => Gain gL2 => mix.left;
ac2[1] => Gain gR2 => mix.right;

// Voice 3
SinOsc c3[2] => ADSR ac3[2];
SinOsc m3[2] => ADSR am3[2] => c3;

ac3[0] => Gain gL3 => mix.left;
ac3[1] => Gain gR3 => mix.right;

[60, 62, 65, 69, 71] @=> int notes[];
[60, 62, 65, 69, 71] @=> int notes2[];
[60, 62, 65, 69, 71] @=> int notes3[];
[2::second, 3::second, 500::ms, 100::ms, 0::ms] @=> dur durs[];
[2::second, 3::second, 500::ms, 100::ms, 0::ms] @=> dur durs2[];
[2::second, 3::second, 500::ms, 100::ms, 0::ms] @=> dur durs3[];
[100, 100, 80, 20, 50] @=> int vels[];
[100, 100, 80, 20, 50] @=> int vels2[];
[100, 100, 80, 20, 50] @=> int vels3[];

1.0 => float randomChoice;
0 => int nextPos;
0.0 => float restChance;
300.0 => float modDepth1;
300.0 => float modDepth2;
300.0 => float modDepth3;


1.0 => float randomChoice2;
0 => int nextPos2;
0.0 => float restChance2;

1.0 => float randomChoice3;
0 => int nextPos3;
0.0 => float restChance3;

Math.srandom(6);

mix.pan(0.);

0.2 => float masterGain => c[0].gain => c[1].gain;
masterGain => c2[0].gain => c2[1].gain => c3[0].gain => c3[1].gain;
2 => c[0].sync => c[1].sync => c2[0].sync => c2[1].sync => c3[0].sync => c3[1].sync;

440 => c[0].freq => c[1].freq; // Carrier Frequency

110 => m[0].freq => m[1].freq; // Modulator Frequency
300 => m[0].gain => m[1].gain; // Modulator Depth

1.6667 => float cmRatio;
1.0 => float spread;

0.125 => float cmRatio2;
1.0 => float spread2;

4.0 => float cmRatio3;
1.0 => float spread3;


// Amplitude and mod depth envelopes
// Voice 1
ac[0].set(500::ms, 500::ms, 0.5, 1::second);
am[0].set(500::ms, 500::ms, 0.5, 1::second);
ac[1].set(500::ms, 500::ms, 0.5, 1::second);
am[1].set(500::ms, 500::ms, 0.5, 1::second);
// Voice 2
ac2[0].set(500::ms, 500::ms, 0.5, 1::second);
am2[0].set(500::ms, 500::ms, 0.5, 1::second);
ac2[1].set(500::ms, 500::ms, 0.5, 1::second);
am2[1].set(500::ms, 500::ms, 0.5, 1::second);
// Voice 3
ac3[0].set(500::ms, 500::ms, 0.5, 1::second);
am3[0].set(500::ms, 500::ms, 0.5, 1::second);
ac3[1].set(500::ms, 500::ms, 0.5, 1::second);
am3[1].set(500::ms, 500::ms, 0.5, 1::second);


// OSC
OscIn oscIn1;
OscMsg msg1;

OscIn oscIn2;
OscMsg msg2;

OscIn oscIn3;
OscMsg msg3;

10010 => oscIn1.port;
10020 => oscIn2.port;
10030 => oscIn3.port;

//oscIn1.addAddress("/globalstart, i");
//oscIn1.addAddress("/mastergain, f");
// Voice 1 Addresses
//oscIn1.addAddress("/voice1_gain_left, f");
//oscIn1.addAddress("/voice1_gain_right, f");
//oscIn1.addAddress("/voice1_notes, i i i i i i i i");
//oscIn1.addAddress("/voice1_vels, i i i i i i i i");
//oscIn1.addAddress("/voice1_durs, i i i i i i i i");
//oscIn1.addAddress("/voice1_random, f");
//oscIn1.addAddress("/voice1_rest, f");
//oscIn1.addAddress("/voice1_ratio, f");
//oscIn1.addAddress("/voice1_spread, f");
//oscIn1.addAddress("/voice1_moddepth, f");
//oscIn1.addAddress("/voice1_attack, i");
//oscIn1.addAddress("/voice1_decay, i");
//oscIn1.addAddress("/voice1_sustain, f");
//oscIn1.addAddress("/voice1_release, i");
// Voice 2 Addresses
//oscIn2.addAddress("/voice2_gain_left, f");
//oscIn2.addAddress("/voice2_gain_right, f");
//oscIn2.addAddress("/voice2_notes, i i i i i i i i");
//oscIn2.addAddress("/voice2_vels, i i i i i i i i");
//oscIn2.addAddress("/voice2_durs, i i i i i i i i");
//oscIn2.addAddress("/voice2_random, f");
//oscIn2.addAddress("/voice2_rest, f");
//oscIn2.addAddress("/voice2_ratio, f");
//oscIn2.addAddress("/voice2_spread, f");
//oscIn2.addAddress("/voice2_moddepth, f");
//oscIn2.addAddress("/voice2_attack, i");
//oscIn2.addAddress("/voice2_decay, i");
//oscIn2.addAddress("/voice2_sustain, f");
//oscIn2.addAddress("/voice2_release, i");
// Voice 3 Addresses
//oscIn3.addAddress("/voice3_gain_left, f");
//oscIn3.addAddress("/voice3_gain_right, f");
//oscIn3.addAddress("/voice3_notes, i i i i i i i i");
//oscIn3.addAddress("/voice3_vels, i i i i i i i i");
//oscIn3.addAddress("/voice3_durs, i i i i i i i i");
//oscIn3.addAddress("/voice3_random, f");
//oscIn3.addAddress("/voice3_rest, f");
//oscIn3.addAddress("/voice3_ratio, f");
//oscIn3.addAddress("/voice3_spread, f");
//oscIn3.addAddress("/voice3_moddepth, f");
//oscIn3.addAddress("/voice3_attack, i");
//oscIn3.addAddress("/voice3_decay, i");
//oscIn3.addAddress("/voice3_sustain, f");
//oscIn3.addAddress("/voice3_release, i");

oscIn1.listenAll();
oscIn2.listenAll();
oscIn3.listenAll();

fun void adjustGain(Gain g, float newGain) {
	(newGain - g.gain()) * 0.04 => float deltaGain;

	now + 25::ms => time endTime;

	while (now < endTime) {
		g.gain() + deltaGain => g.gain;
		1::ms => now;
	}
}


fun void playNote1() {
	float velocity;
	dur duration;
	int midiNote;
	
	if (Math.randomf() <= randomChoice) {
		vels[Math.random2(0, vels.size() - 1)] * 0.01 * masterGain => velocity;
		durs[Math.random2(0, durs.size() - 1)] => duration;
		notes[Math.random2(0, notes.size() - 1)] => midiNote;
	}
	else {
		vels[nextPos] * 0.01 * masterGain => velocity;
		durs[nextPos] => duration;
		notes[nextPos] => midiNote;
	}

	((nextPos + 1) % notes.cap()) => nextPos;
	
	Std.mtof(midiNote) => float cFreq;
	cFreq * cmRatio => float mFreq;

	velocity => c[0].gain => c[1].gain;
	
	cFreq => c[0].freq;
	cFreq + spread => c[1].freq;
	mFreq => m[0].freq;
	mFreq + spread => m[1].freq;

	modDepth1 => m[0].gain => m[1].gain;
	
	ac[0].keyOn();
	ac[1].keyOn();
	am[0].keyOn();
	am[1].keyOn();

	duration => now;

	ac[0].keyOff();
	am[0].keyOff();
	ac[1].keyOff();
	am[1].keyOff();

	ac[0].releaseTime() => now;

	if (Math.randomf() <= restChance) {	
    	durs[Math.random2(0, durs.cap() - 1)] => now;
    }	
    else {        
        10::ms => now;
    }
}

fun void playNote2() {
	float velocity;
	dur duration;
	int midiNote;
	
	if (Math.randomf() <= randomChoice2) {
		vels2[Math.random2(0, vels2.size() - 1)] * 0.01 * masterGain => velocity;
		durs2[Math.random2(0, durs2.size() - 1)] => duration;
		notes2[Math.random2(0, notes2.size() - 1)] => midiNote;
	}
	else {
		vels2[nextPos2] * 0.01 * masterGain => velocity;
		durs2[nextPos2] => duration;
		notes2[nextPos2] => midiNote;
	}

	((nextPos2 + 1) % notes2.cap()) => nextPos2;
	
	Std.mtof(midiNote) => float cFreq;
	cFreq * cmRatio2 => float mFreq;

	velocity => c2[0].gain => c2[1].gain;
	
	cFreq => c2[0].freq;
	cFreq + spread2 => c2[1].freq;
	mFreq => m2[0].freq;
	mFreq + spread2 => m2[1].freq;

	modDepth2 => m2[0].gain => m2[1].gain;
	
	ac2[0].keyOn();
	ac2[1].keyOn();
	am2[0].keyOn();
	am2[1].keyOn();

	duration => now;

	ac2[0].keyOff();
	am2[0].keyOff();
	ac2[1].keyOff();
	am2[1].keyOff();

	ac2[0].releaseTime() => now;

	if (Math.randomf() <= restChance2) {	
    	durs2[Math.random2(0, durs2.cap() - 1)] => now;
    }	
    else {        
        10::ms => now;
    }
}

fun void playNote3() {
	float velocity;
	dur duration;
	int midiNote;
	
	if (Math.randomf() <= randomChoice3) {
		vels3[Math.random2(0, vels3.size() - 1)] * 0.01 * masterGain => velocity;
		durs3[Math.random2(0, durs3.size() - 1)] => duration;
		notes3[Math.random2(0, notes3.size() - 1)] => midiNote;
	}
	else {
		vels3[nextPos3] * 0.01 * masterGain => velocity;
		durs3[nextPos3] => duration;
		notes3[nextPos3] => midiNote;
	}

	((nextPos3 + 1) % notes3.cap()) => nextPos3;
	
	Std.mtof(midiNote) => float cFreq;
	cFreq * cmRatio3 => float mFreq;

	velocity => c3[0].gain => c3[1].gain;
	
	cFreq => c3[0].freq;
	cFreq + spread3 => c3[1].freq;
	mFreq => m3[0].freq;
	mFreq + spread3 => m3[1].freq;

	modDepth3 => m3[0].gain => m3[1].gain;
	
	ac3[0].keyOn();
	ac3[1].keyOn();
	am3[0].keyOn();
	am3[1].keyOn();

	duration => now;

	ac3[0].keyOff();
	am3[0].keyOff();
	ac3[1].keyOff();
	am3[1].keyOff();

	ac3[0].releaseTime() => now;

	if (Math.randomf() <= restChance3) {	
    	durs3[Math.random2(0, durs3.cap() - 1)] => now;
    }	
    else {        
        10::ms => now;
    }
}


fun void playNotes1() {
    while (true) {
        playNote1();
    }
}

fun void playNotes2() {
	while (true) {
		playNote2();
	}
}

fun void playNotes3() {
	while (true) {
		playNote3();
	}
}

null => Shred @ mainPlayLoop1;
null => Shred @ mainPlayLoop2;
null => Shred @ mainPlayLoop3;

fun void monitorOsc1() {
	while (true) {
	oscIn1 => now;
	<<< "OSC 1 has message" >>>;
	while (oscIn1.recv(msg1)) {
		<<< "msg: ", msg1.address >>>;
		if (msg1.address == "/voice1_gain_left") {
			spork ~ adjustGain(gL, msg1.getFloat(0));
		}
		else if (msg1.address == "/voice1_gain_right") {
			spork ~ adjustGain(gR, msg1.getFloat(0));
		}
        else if (msg1.address == "/voice1_notes") {
        	<<< "Receiving notes" >>>;
            notes.reset();
            msg1.getInt(0) => int nextNote;
            0 => int i;
            while (nextNote != 0) {
                <<< "Note: ", nextNote >>>;
            	nextNote => notes[i];
                i + 1 => i;
                msg1.getInt(i) => nextNote;
            }
            <<< "i: ", i >>>;
            notes.size(i - 1);
        }
        else if (msg1.address == "/voice1_durs") {
            <<< "Receiving durs" >>>;
            durs.reset();
            msg1.getInt(0) => int nextDur;
            0 => int i;
            while (nextDur != 0) {
                <<< "Dur: ", nextDur >>>;
                nextDur::ms => durs[i];
                i + 1 => i;
                msg1.getInt(i) => nextDur;
            }
            <<< "i: ", i >>>;
            durs.size(i - 1);
        }
        else if (msg1.address == "/voice1_vels") {
            <<< "Receiving vels" >>>;
            vels.reset();
            msg1.getInt(0) => int nextVel;
            0 => int i;
            while (nextVel != 0) {
                <<< "Note: ", nextVel >>>;
                nextVel => vels[i];
                i + 1 => i;
                msg1.getInt(i) => nextVel;
            }
            <<< "i: ", i >>>;
            vels.size(i - 1);
        }
		else if (msg1.address == "/voice1_attack") {
			msg1.getInt(0) => int attack;
			attack::ms => ac[0].attackTime => ac[1].attackTime;
            attack::ms => am[0].attackTime => am[1].attackTime;
		}
		else if (msg1.address == "/voice1_decay") {
			msg1.getInt(0) => int decay;
			decay::ms => ac[0].decayTime => ac[1].decayTime;
            decay::ms => am[0].decayTime => am[1].decayTime;
		}
		else if (msg1.address == "/voice1_sustain") {
			msg1.getFloat(0) => float sustain;
			sustain => ac[0].sustainLevel => ac[1].sustainLevel;
            sustain => am[0].sustainLevel => am[1].sustainLevel;
		}
		else if (msg1.address == "/voice1_release") {
			msg1.getInt(0) => int release;
			release::ms => ac[0].releaseTime => ac[1].releaseTime;
            release::ms => am[0].releaseTime => am[1].releaseTime;
		}
		else if (msg1.address == "/voice1_spread") {
			msg1.getFloat(0) => spread;
		}
		else if (msg1.address == "/voice1_ratio") {
			msg1.getFloat(0) => cmRatio;
		}
		else if (msg1.address == "/voice1_moddepth") {
			msg1.getFloat(0) => modDepth1;
		}
        else if (msg1.address == "/voice1_random") {
            msg1.getFloat(0) => randomChoice;
        }
        else if (msg1.address == "/voice1_rest") {
            msg1.getFloat(0) => restChance;
        }
		else if (msg1.address == "/mastergain") {
        	msg1.getFloat(0) => masterGain;
        }
        else if (msg1.address == "/globalstart") {
            if (msg1.getInt(0) != 0) {
            	spork ~ playNotes1() @=> mainPlayLoop1;
            	spork ~ playNotes2() @=> mainPlayLoop2;
            	spork ~ playNotes3() @=> mainPlayLoop3;				
            }
            else {
				if (mainPlayLoop1 != null) mainPlayLoop1.exit();
				if (mainPlayLoop2 != null) mainPlayLoop2.exit();
				if (mainPlayLoop3 != null) mainPlayLoop3.exit();
            }
        }
	}
}
}

fun void monitorOsc2() {
	while (true) {
	oscIn2 => now;
	<<< "OSC 2 has message" >>>;
		while (oscIn2.recv(msg2)){
			<<< "msg: ", msg2.address >>>;
	// Voice 2
		if (msg2.address == "/voice2_gain_left") {
			spork ~ adjustGain(gL2, msg2.getFloat(0));
		}
		else if (msg2.address == "/voice2_gain_right") {
			spork ~ adjustGain(gR2, msg2.getFloat(0));
		}
        else if (msg2.address == "/voice2_notes") {
        	<<< "Receiving notes" >>>;
            notes2.reset();
            msg2.getInt(0) => int nextNote;
            0 => int i;
            while (nextNote != 0) {
                <<< "Note: ", nextNote >>>;
            	nextNote => notes2[i];
                i + 1 => i;
                msg2.getInt(i) => nextNote;
            }
            <<< "i: ", i >>>;
            notes2.size(i - 1);
        }
        else if (msg2.address == "/voice2_durs") {
            <<< "Receiving durs" >>>;
            durs2.reset();
            msg2.getInt(0) => int nextDur;
            0 => int i;
            while (nextDur != 0) {
                <<< "Dur: ", nextDur >>>;
                nextDur::ms => durs2[i];
                i + 1 => i;
                msg2.getInt(i) => nextDur;
            }
            <<< "i: ", i >>>;
            durs2.size(i - 1);
        }
        else if (msg2.address == "/voice2_vels") {
            <<< "Receiving vels" >>>;
            vels2.reset();
            msg2.getInt(0) => int nextVel;
            0 => int i;
            while (nextVel != 0) {
                <<< "Note: ", nextVel >>>;
                nextVel => vels2[i];
                i + 1 => i;
                msg2.getInt(i) => nextVel;
            }
            <<< "i: ", i >>>;
            vels2.size(i - 1);
        }
		else if (msg2.address == "/voice2_attack") {
			msg2.getInt(0) => int attack;
			attack::ms => ac2[0].attackTime => ac2[1].attackTime;
            attack::ms => am2[0].attackTime => am2[1].attackTime;
		}
		else if (msg2.address == "/voice2_decay") {
			msg2.getInt(0) => int decay;
			decay::ms => ac2[0].decayTime => ac2[1].decayTime;
            decay::ms => am2[0].decayTime => am2[1].decayTime;
		}
		else if (msg2.address == "/voice2_sustain") {
			msg2.getFloat(0) => float sustain;
			sustain => ac2[0].sustainLevel => ac2[1].sustainLevel;
            sustain => am2[0].sustainLevel => am2[1].sustainLevel;
		}
		else if (msg2.address == "/voice2_release") {
			msg2.getInt(0) => int release;
			release::ms => ac2[0].releaseTime => ac2[1].releaseTime;
            release::ms => am2[0].releaseTime => am2[1].releaseTime;
		}
		else if (msg2.address == "/voice2_spread") {
			msg2.getFloat(0) => spread2;
		}
		else if (msg2.address == "/voice2_ratio") {
			msg2.getFloat(0) => cmRatio2;
		}
		else if (msg2.address == "/voice2_moddepth") {
			msg2.getFloat(0) => modDepth2;
		}
        else if (msg2.address == "/voice2_random") {
            msg2.getFloat(0) => randomChoice2;
        }
        else if (msg2.address == "/voice2_rest") {
            msg2.getFloat(0) => restChance2;
        }
	}
}
}

fun void monitorOsc3() {
	while (true) {
	oscIn3 => now;
	<<< "OSC 3 has message" >>>;
		while (oscIn3.recv(msg3)){
			<<< "msg: ", msg3.address >>>;
	// Voice 2
		if (msg3.address == "/voice3_gain_left") {
			spork ~ adjustGain(gL3, msg3.getFloat(0));
		}
		else if (msg3.address == "/voice3_gain_right") {
			spork ~ adjustGain(gR3, msg3.getFloat(0));
		}
        else if (msg3.address == "/voice3_notes") {
        	<<< "Receiving notes" >>>;
            notes3.reset();
            msg3.getInt(0) => int nextNote;
            0 => int i;
            while (nextNote != 0) {
                <<< "Note: ", nextNote >>>;
            	nextNote => notes3[i];
                i + 1 => i;
                msg3.getInt(i) => nextNote;
            }
            <<< "i: ", i >>>;
            notes3.size(i - 1);
        }
        else if (msg3.address == "/voice3_durs") {
            <<< "Receiving durs" >>>;
            durs3.reset();
            msg3.getInt(0) => int nextDur;
            0 => int i;
            while (nextDur != 0) {
                <<< "Dur: ", nextDur >>>;
                nextDur::ms => durs3[i];
                i + 1 => i;
                msg3.getInt(i) => nextDur;
            }
            <<< "i: ", i >>>;
            durs3.size(i - 1);
        }
        else if (msg3.address == "/voice3_vels") {
            <<< "Receiving vels" >>>;
            vels3.reset();
            msg3.getInt(0) => int nextVel;
            0 => int i;
            while (nextVel != 0) {
                <<< "Note: ", nextVel >>>;
                nextVel => vels3[i];
                i + 1 => i;
                msg3.getInt(i) => nextVel;
            }
            <<< "i: ", i >>>;
            vels3.size(i - 1);
        }
		else if (msg3.address == "/voice3_attack") {
			msg3.getInt(0) => int attack;
			attack::ms => ac3[0].attackTime => ac3[1].attackTime;
            attack::ms => am3[0].attackTime => am3[1].attackTime;
		}
		else if (msg3.address == "/voice3_decay") {
			msg3.getInt(0) => int decay;
			decay::ms => ac3[0].decayTime => ac3[1].decayTime;
            decay::ms => am3[0].decayTime => am3[1].decayTime;
		}
		else if (msg3.address == "/voice3_sustain") {
			msg3.getFloat(0) => float sustain;
			sustain => ac3[0].sustainLevel => ac3[1].sustainLevel;
            sustain => am3[0].sustainLevel => am3[1].sustainLevel;
		}
		else if (msg3.address == "/voice3_release") {
			msg3.getInt(0) => int release;
			release::ms => ac3[0].releaseTime => ac3[1].releaseTime;
            release::ms => am3[0].releaseTime => am3[1].releaseTime;
		}
		else if (msg3.address == "/voice3_spread") {
			msg3.getFloat(0) => spread3;
		}
		else if (msg3.address == "/voice3_ratio") {
			msg3.getFloat(0) => cmRatio3;
		}
		else if (msg3.address == "/voice3_moddepth") {
			msg3.getFloat(0) => modDepth3;
		}
        else if (msg3.address == "/voice3_random") {
            msg3.getFloat(0) => randomChoice3;
        }
        else if (msg3.address == "/voice3_rest") {
            msg3.getFloat(0) => restChance3;
        }
	}
}
}

spork ~ monitorOsc1();
spork ~ monitorOsc2();
spork ~ monitorOsc3();

while (true) {
	1::day => now;	
}