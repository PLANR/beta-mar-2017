# ethel-agnes-vera

Spatialized composition for three Raspberry Pis, each connect to two speakers.

Spatialization occurs in the form of Manifold Interface Amplitude Panning (MIAP), which supposes gain structures based on a collection of trianges and a sound that traverses over a plane. The composition relies on spatial changes over time as it's primary element, and has an evolving speaker placement as the Nodes of the MIAP instance change.

The sound source is fairly arbitrary, as long as it is complex enough to perceive in an XY space.


![miap](miap-image.png)

