
string fn;
for (int i; i < 100; i++) {
  "" => fn;
  if (i > 10) {
    "0" + i +=> fn;
  }
  else {
    "00" + i +=> fn;
  }
  spork ~ play(fn);
  Math.random2f(1, 2)::second => now;
}

fun void play(string fn) {
  SndBuf buf;
  Step step => Gain g => ADSR adsr => dac;
  /* g.gain(0.9); */

  buf.read("./data/audio/" + fn + ".wav");
  second => now;
  buf.samples() => int s;
  float data[s];
  for (int i; i < s; i++) {
    buf.valueAt(i) => data[i];
  }

  second => now;
  (s * 0.05)::samp => adsr.attackTime;
  (s * 0.9)::samp => adsr.decayTime;
  (s * 0.05)::samp => adsr.releaseTime;

  1 => adsr.keyOn;
  1 => int off;
  for (int i; i < s; i++) {
    data[i] => step.next;

    if (off && i >= s * 0.95) {
      1 => adsr.keyOff;
      0 => off;
    }
    0.5::samp => now;
  }
}
