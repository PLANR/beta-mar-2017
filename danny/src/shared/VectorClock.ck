class VectorParser { 
  fun static VectorClock parse(string input) {
    find_opening_brace(0, input) + 1 => int start_pos => int cur_pos;
    find_closing_brace(0, input) => int end_pos;

    "key" => string stage;
    ParsePair result[0];
    while (cur_pos < end_pos) {
      if (stage == "key") {
        find_opening_quote(cur_pos, input) + 1 => int key_start;
        find_closing_quote(key_start, input) => int key_end;
        result << ParsePair.create(input.substring(key_start, key_end - key_start), 0);
        
        // update parser: ignore colons
        key_end + 1 => cur_pos;
        "value" => stage;
      }
      else if (stage == "value") {
        find_semi(cur_pos, input) => int val_end;
        result[result.size() - 1].count(Std.atoi(input.substring(cur_pos + 1, val_end - cur_pos)));

        val_end + 1 => cur_pos;
        "key" => stage;
      }
    }
    return VectorClockMapper.pair_to_vc(result);
  }

  fun static int find_closing_brace(int start, string input) {
    return find_string(start, "]", input);
  }

  fun static int find_opening_brace(int start, string input) {
    return find_string(start, "[", input);
  }

  fun static int find_opening_quote(int start, string input) {
    return find_string(start, "'", input);
  }

  fun static int find_closing_quote(int start, string input) {
    return find_string(start, "'", input);
  }

  fun static int find_semi(int start, string input) {
    return find_string(start, ";", input);
  }

  fun static int find_string(int start, string tgt, string input) {
    return input.find(tgt, start);
  }
}

class VectorClockMapper {
  fun static VectorClock pair_to_vc(ParsePair pp[]) {
    VectorClock.create() @=> VectorClock out;
    for (int i; i < pp.size(); i++) {
      out.set_value(pp[i].key(), pp[i].count()) @=> out;
    }
    return out;
  }
}

/*
 * Simple Vector clock implementation
 */
public class VectorClock {
  int m_history[0]; // associative array
  StringSet m_keys;

  fun static VectorClock create() {
    new VectorClock @=> VectorClock out;
    int nu_hist[0];
    out.init(StringSet.create(), nu_hist);
    return out;
  }

  fun static VectorClock from_string(string vc_string) {
    return VectorParser.parse(vc_string);    
  }

  fun VectorClock init(StringSet keys, int history[]) {
    StringSet.create() @=> m_keys;
    copy_over(keys, history);
    return this;
  }

  fun StringSet keys() {
    return m_keys.copy();
  }

  fun VectorClock merge(VectorClock other) {
    this.keys() @=> StringSet this_keys;
    other.keys() @=> StringSet other_keys;
    other_keys.union(this_keys) @=> StringSet merged_keys;

    int merged_history[0];
    string key;
    merged_keys.size() => int m_size;
    for (int i; i < m_size; i++) {
      merged_keys.at(i) => key;

      // take greater of two
      if (other_keys.includes(key) && this_keys.includes(key)) {
        this.at(key) => int this_val;
        other.at(key) => int other_val;
        if (this_val < other_val) {
          other_val => merged_history[key];
        }
        else {
          this_val => merged_history[key];
        }
      }

      // take the one that exists
      else if (other_keys.includes(key)) {
        other.at(key) => merged_history[key];
      }
      else {
        this.at(key) => merged_history[key];
      }
    }

    VectorClock.create() @=> VectorClock out;
    out.init(merged_keys, merged_history);
    return out;
  }
  
  fun VectorClock set_value(string key, int value) {
    VectorClock.create() @=> VectorClock out;
    m_keys.add(key) @=> StringSet nu_keys;
    int nu_history[0];
    for (int i; i < nu_keys.size(); i++) {
      if (nu_keys.at(i) == key)
        value => nu_history[key];
      else
        m_history[nu_keys.at(i)] => nu_history[nu_keys.at(i)];
    }
    return out.init(nu_keys, nu_history);
  }

  fun VectorClock add_event(string key) {
    at(key) => int old_val;
    return set_value(key, old_val + 1);
  }

  fun int at(string key) {
    return m_history[key];
  }
  
  fun string toString() {
    "[" => string out;
    for (int i; i < m_keys.size(); i++) {
      "'" + m_keys.at(i) + "':" +=> out;
      Std.itoa(m_history[m_keys.at(i)]) + ";" +=> out;
    }
    "]" +=> out;
    return out;
  }

  private void copy_keys(StringSet keys) {
    m_keys.union(keys) @=> m_keys;
  }

  private void copy_history(StringSet keys, int history[]) {
    for (int i; i < keys.size(); i++) {
      history[keys.at(i)] => m_history[m_keys.at(i)];
    }
  }

  private void copy_over(StringSet keys, int history[]) {
    copy_keys(keys);
    copy_history(keys, history);
  }
}
