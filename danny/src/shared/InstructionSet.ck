public class InstructionSet {
  string m_move;
  VectorClock m_vc;

  fun static InstructionSet create(string move, VectorClock vc) {
    new InstructionSet @=> InstructionSet out;
    return out.init(move, vc);
  }

  fun InstructionSet init(string move, VectorClock vc) {
    move => m_move;
    vc @=> m_vc;
    return this;
  }

  fun string movement() { return m_move; }
  fun VectorClock history() { return m_vc; }

  fun InstructionSet set_history(VectorClock nu) {
    return InstructionSet.create(m_move, m_vc.merge(nu));
  }
}
