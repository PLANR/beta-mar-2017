public class ParsePair {
  string m_key;
  int m_count;
  fun static ParsePair create(string k, int c) {
    new ParsePair @=> ParsePair out;
    return out.init(k, c);
  }

  fun ParsePair init(string k, int c) {
    k => m_key;
    c => m_count;
    return this;
  }

  fun int count() { return m_count; }
  fun int count(int nu) { return nu => m_count; }
  fun string key() { return m_key; }
  fun string key(string nu) { return nu => m_key; }
  fun string toString() {
    "{ " + "\"" + m_key + "\"" + ": " + Std.itoa(m_count) + " }" => string out;
    return out;
  }
}

