public class StringSet {
  string m_contents[];

  fun static StringSet create() {
    new StringSet @=> StringSet out;
    string xs[0];
    return out.init(xs);
  }

  fun StringSet init(string values[]) {
    new string[values.size()] @=> m_contents;
    for (int i; i < values.size(); i++) {
      values[i] => m_contents[i];
    }
    return this;
  }

  fun string at(int idx) {
    if (idx < this.size()) {
      return this.m_contents[idx];
    }
    else {
      return NULL;
    }
  }

  fun int size() {
    return this.m_contents.size();
  }

  fun StringSet add(string val) {
    if (this.includes(val)) {
      return this;
    }
    else {
      StringSet.create() @=> StringSet out;
      string vals[this.size()];
      for (int i; i < this.size(); i++) {
        m_contents[i] => vals[i];
      }
      vals << val;
      out.init(vals);
      return out;
    }
  }

  fun StringSet remove(string val) {
    if (!this.includes(val)) {
      return this;
    }
    else {
      this.index_of(val) => int v_idx;
      if (v_idx > -1) {
        string vals[this.size()];
        for (int i; i < vals.size(); i++) {
          if (i != v_idx) this.at(i) => vals[i];
        }
        StringSet.create() @=> StringSet out;
        out.init(vals);
        return out;
      }
      else {
        return this;
      }
    }
  }

  /* members that are in either set */
  fun StringSet union(StringSet other) {
    StringSet.create() @=> StringSet out;
    for (int i; i < this.size(); i++) {
      out.add(this.at(i)) @=> out;
    }
    for (int i; i < other.size(); i++) {
      out.add(other.at(i)) @=> out;
    }
    return out;
  }

  /* members in both sets */
  fun StringSet intersection(StringSet other) {
    StringSet.create() @=> StringSet out;
    for (int i; i < this.size(); i++) {
      if (other.includes(this.at(i))) {
        out.add(this.at(i)) @=> out;
      }
    }
    for (int i; i < other.size(); i++) {
      if (this.includes(this.at(i))) {
        out.add(other.at(i)) @=> out;
      }
    }
    return out;
  }

  /* members in this, but not in other */
  fun StringSet subtract(StringSet other) {
    StringSet.create() @=> StringSet out;
    for (int i; i < this.size(); i++) {
      if (!other.includes(this.at(i))) {
        out.add(this.at(i)) @=> out;
      }
    }
    return out;
  }

  fun int includes(string val) {
    for (int i; i < this.size(); i++) {
      if (this.at(i) == val) return 1;
    }
    return 0;
  }

  fun StringSet copy() {
    StringSet.create() @=> StringSet out;
    out.union(this) @=> out;
    return out;
  }

  fun string toString() {
    "{ " => string out;
    for (int i; i < this.size(); i++) {
      this.at(i).toString() + " " +=> out;
    }
    "}" +=> out;
    return out;
  }

  private int index_of(string val) {
    for (int i; i < this.size(); i++) {
      if (val == this.at(i)) return i;
    }
    return -1;
  }
}

/* Tests

<<< "instantiating" , "" >>>;
StringSet.create() @=> StringSet a;
StringSet.create() @=> StringSet b;

<<< "adding", "" >>>;
a.add("1").add("2").add("2") @=> StringSet c;
b.add("2").add("3").add("4") @=> StringSet d;

<<< "a: ", a.toString(), "" >>>;
<<< "b: ", b.toString(), "" >>>;
<<< "c: ", c.toString(), "" >>>;
<<< "d: ", d.toString(), "" >>>;
<<< "a /\\ c: ", a.union(c).toString(), "" >>>;
<<< "c /\\ d: ", c.union(d).toString(), "" >>>;
<<< "c \\/ a: ", c.intersection(a).toString(), "" >>>;
<<< "c - d: ", c.subtract(d).toString(), "" >>>;
<<< "d - d: ", d.subtract(d).toString(), "" >>>;
<<< "d - c: ", d.subtract(c).toString(), "" >>>;
<<< "d - a: ", d.subtract(a).toString(), "" >>>;
*/
