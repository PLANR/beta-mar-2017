public class Message {
  string m_move;
  string m_history;
  
  fun string movement() { return m_move; }
  fun string history() { return m_history; }

  fun string movement(string m) {
    return m => m_move;
  }

  fun string history(string m) {
    return m => m_history;
  }
}
