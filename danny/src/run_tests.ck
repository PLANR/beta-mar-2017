/*
 * Load requirements
 */

Machine.add("./Transforms.ck");
ms => now;
Machine.add("./StringSet.ck");
ms => now;
Machine.add("./Player.ck");
ms => now;
Machine.add("./VectorClock.ck");
ms => now;

/* run test */
Machine.add("./tests/TransformTests.ck");
ms => now;
Machine.add("./tests/PlayerTests.ck");
ms => now;
Machine.add("./tests/VectorClockTests.ck");
ms => now;
me.exit();
