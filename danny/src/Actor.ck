public class Actor {
  string m_name;

  Parse m_parse;
  Update m_update;
  Play m_play;
  Send m_send;
  Receive m_recv;
  ActorFinished m_done;

  Player m_player;
  HistoryManager m_updater;
  InstructionSetParser m_parser; 
  Sender m_sender;
  Receiver m_receiver;
  ConnMapper m_mapper;
  
  fun void init(string here_dir, string name) {
    m_mapper.init();
    m_receiver.init(name, m_mapper);
    m_updater.init(name);
    m_sender.init(name, m_mapper);
    m_player.init(here_dir, get_channel(name));
    second => now;
    m_updater.init(name);
    name => m_name;
  }

  fun void run(Event s) {
    spork ~ m_updater.listen(m_update, m_play);
    spork ~ m_receiver.listen(m_recv, m_parse);
    spork ~ m_parser.listen(m_parse, m_update);
    spork ~ m_player.listen(m_play, m_send, m_done);
    spork ~ m_sender.listen(m_send, m_recv);
    ms => now;
    s => now;
    VectorClock.from_string("['"+m_name+"':1;]") @=> VectorClock initial;
    InstructionSet.create("forward", initial) @=> InstructionSet start;
    m_play.instructions(start);
    m_play.signal();
    m_done => now;
    me.exit();
  }
  
  fun int get_channel(string name) {
    return Std.atoi(name.substring(name.length() - 1, 1));
  }
}
