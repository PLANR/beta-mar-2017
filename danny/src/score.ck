<<< "hello world" ,"" >>>;

me.arg(0) => string here;
Std.atoi(me.arg(1)) => int num_actors;

if (num_actors < 2) {
  <<< "Please provide at least 2 actors as an argument", "" >>>;
  me.exit();
}
<<< "starting with ", num_actors, " actors", "" >>>;

<<< "naming actors", "" >>>;
string actor_names[0];
for (int i; i < num_actors; i++) {
  actor_names << me.arg(2 + i);
}

<<< "building actors", "" >>>;
Actor actors[num_actors];
for (int i; i < num_actors; i++) {
  new Actor @=> actors[i];
  actors[i].init(here, actor_names[i]);
}

<<< "starting actors", "" >>>;
Event s;
for (int i; i < num_actors; i++) {
  spork ~ actors[i].run(s);
  ms => now;
}
s.broadcast();
while (ms => now);
