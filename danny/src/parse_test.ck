"['a':10; 'b':2; 'cats':3;]" => string sample_input;
print_arr(parse(sample_input));

fun void print_arr(ParsePair in[]) {
  for (int i; i < in.size(); i++) {
    <<< in[i].toString(), "" >>>;
  }
}

fun ParsePair[] parse(string input) {
  find_opening_brace(0, input) + 1 => int start_pos => int cur_pos;
  find_closing_brace(0, input) => int end_pos;
  "key" => string stage;
  ParsePair result[0];
  while (cur_pos < end_pos) {
    if (stage == "key") {
      find_opening_quote(cur_pos, input) + 1 => int key_start;
      find_closing_quote(key_start, input) => int key_end;
      result << ParsePair.create(input.substring(key_start, key_end - key_start), 0);
      
      // update parser: ignore colons
      key_end + 1 => cur_pos;
      "value" => stage;
    }
    else if (stage == "value") {
      find_semi(cur_pos, input) => int val_end;
      result[result.size() - 1].count(Std.atoi(input.substring(cur_pos + 1, val_end - cur_pos)));

      val_end + 1 => cur_pos;
      "key" => stage;
    }
  }
  return result;
}

fun int find_closing_brace(int start, string input) {
  return find_string(start, "]", input);
}

fun int find_opening_brace(int start, string input) {
  return find_string(start, "[", input);
}

fun int find_opening_quote(int start, string input) {
  return find_string(start, "'", input);
}

fun int find_closing_quote(int start, string input) {
  return find_string(start, "'", input);
}

fun int find_semi(int start, string input) {
  return find_string(start, ";", input);
}

fun int find_string(int start, string tgt, string input) {
  return input.find(tgt, start);
}
