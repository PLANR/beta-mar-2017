public class Parse extends Event {
  Message m_msg;
  
  fun Message message() { return m_msg; }
  fun Message message(Message msg) {
    return msg @=> m_msg;
  }
}
