public class Update extends Event {
  InstructionSet.create("forward", VectorClock.create()) @=> InstructionSet m_insts;
  
  fun InstructionSet instructions() { return m_insts; }
  fun VectorClock history() { return m_insts.history(); }
  fun InstructionSet instructions(InstructionSet nu) {
    return nu @=> m_insts;
  }
}
