public class Send extends Event {
  InstructionSet m_insts;
  
  fun InstructionSet instructions() { return m_insts; }
  fun InstructionSet instructions(InstructionSet nu) {
    return nu @=> m_insts;
  }
}
