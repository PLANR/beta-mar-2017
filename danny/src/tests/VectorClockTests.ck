<<< "------- Vector Clock Tests", "" >>>;

VectorClock vc1;
VectorClock vc2;

<<< "creating histories" >>>;
int history1[0];
int history2[0];

1 => history1["a"];
2 => history1["b"];

2 => history2["a"];
1 => history2["b"];
3 => history2["c"];

<<< "creating sets" >>>;
StringSet.create().add("a").add("b") @=> StringSet keys1;
StringSet.create().add("b").add("b").add("c") @=> StringSet keys2;

<<< "initializing vcs" >>>;
vc1.init(keys1, history1);
vc2.init(keys2, history2);

vc1.merge(vc2) @=> VectorClock vc3;
<<< "merging vector clocks has keys from both and greatest counts", 
    vc3.at("a") == 2, vc3.at("b") == 2, vc3.at("c") == 3 >>>;
