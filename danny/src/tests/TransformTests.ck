/* tests for Transforms */
<<< "Transform Tests" , "" >>>;

Identity it;
<<< "Identity just passes value through", it.apply(10, 1) == 1, "" >>>;

WeakenOdds wo;
wo.init(0.1);
<<< "WeakenOdds diminishes values at odd positions by set amount", 
    wo.apply(1, 1) == 0.9, "" >>>;
<<< "WeakenOdds leaves values at even positions alone", 
    wo.apply(2, 1) == 1, "" >>>;

WeakenEvens we;
we.init(0.1);
<<< "WeakenEvens diminishes values at even positions by set amount", 
    we.apply(1, 1) == 1, "" >>>;
<<< "WeakenEvens leaves values at odd positions alone", 
    we.apply(2, 1) == 0.9, "" >>>;
