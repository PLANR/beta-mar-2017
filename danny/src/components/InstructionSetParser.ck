public class InstructionSetParser {
  fun InstructionSet parse(Message msg) {
    msg.movement() => string move;

    VectorClock.from_string(msg.history()) @=> VectorClock vc;
    return InstructionSet.create(move, vc);
  }

  fun void listen(Parse pe, Update ue) {
    while (true) {
      pe => now;
      ue.instructions(this.parse(pe.message()));
      ue.signal();
    }
  }
}
