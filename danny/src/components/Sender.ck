public class Sender {
  Std.atoi(Std.getenv("TEST")) => int is_test;
  string m_name;
  ConnMapper m_map;
  int m_send_count;
  114 => int MAX;

  fun void init(string name, ConnMapper map) {
    name => m_name;
    map @=> m_map;
    0 => m_send_count;
  }

  fun void listen(Send se, Receive re) {
    while (true) {
      /* <<< m_name, " send listen loop", "" >>>; */
      se => now;
      /* <<< m_name, " send do", "" >>>; */
      (m_send_count * Math.random2f(0.01, 0.1))::ms => now;
      se.instructions().history().toString() => string msg;
      /* <<< m_name, "sending", msg >>>; */
      send_message(msg);
      1 +=> m_send_count;
      re.broadcast();
    }
  }
  
  private void send_message(string history) {
    OscOut out;
    choose_target() => string tgt;
    <<< m_name, "sending to", tgt>>>;
    out.dest(m_map.ip_for(tgt), m_map.port_for(tgt));
    out.start(tgt);
    out.add(choose_movement());
    out.add(history);
    out.send();
  }

  private string choose_target() {
    m_map.size() => int map_size;
    Math.random2(0, map_size - 1) => int choice;
    m_map.keys().at(choice) => string choice_str;
    while (choice_str == m_name || choice_str.substring(0, choice_str.length() - 1) == "local") {
      Math.random2(0, map_size - 1) => choice;
      m_map.keys().at(choice) => choice_str;
    }
    return choice_str;
  }
  
  private string choose_movement() {
    Math.random2f(0.0, 1.0) => float choice;
    if (choice > 0.9) {
      return "backward";
    }
    else {
      return "forward";
    }
  }

  private int is_local_check(string name) {
    if (is_test) {
      name.substring(0, name.length() - 1) => string sub;
      return sub == "local";
    }
    else {
      return 0;
    }
  }
}
