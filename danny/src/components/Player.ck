class PlayerDone extends Event {}

/*
 * Class that manages playing sound bufs
 */
public class Player extends Chubgraph {
  "../data/audio/" => string SND_FILES;
  0 => int m_step_pos;
  0 => int m_buf_num;
  PlayerDone m_done;

  SndBuf m_bufs[40];
  SndBuf cur_buf;
  Step m_stepper => ADSR m_env => Gain m_gain;
  m_gain.gain(0.75);

  PositionalTransform m_transforms[0];
  TxMapper m_mapper;

  fun static Player create(string here) {
    new Player @=> Player out;
    return out.init(here, 1);
  }

  fun Player init(string here, int channel) {
    if (channel % 2 == 1) {
      m_gain => dac.left;
    }
    else {
      m_gain => dac.right;
    }
    load_sound_files(here);
    configure_env(m_bufs[m_buf_num].samples());
    m_mapper.init();
    return this;
  }
  
  /* spork this */
  fun void listen(Play play, Send send, ActorFinished finished) {
    while (true) {
      <<< "play listen loop", "" >>>;
      play => now;
      <<< "do play", "" >>>;
      interpret_instructions(play.instructions());
      if (is_finished()) {
        <<< "done" >>>;
        finished.signal();
        me.exit();
      }
      else {
        spork ~ this.play();
        /* this.play(); */
        ms => now;
        m_done => now;
        send.instructions(play.instructions());
        send.broadcast();
      }
    }
  }

  fun void play() {
    m_bufs[m_buf_num] @=> cur_buf;
    cur_buf.samples() => int num_samps;
    configure_env(num_samps);
    <<< "Playing", m_buf_num >>>;
    0 => m_step_pos;
    0 => int off;
    float val;
    m_env.keyOn(1);
    for (int i; i < num_samps; i++) {
      if (!off && i >= num_samps * 0.99) {
        1 => off;
        m_env.keyOff(1);
      }
      
      transform(i, cur_buf.valueAt(i)) => val;      
      val => m_stepper.next;
      0.5::samp => now;
    }
    m_done.signal();
  }

  private float transform(int pos, float val) {
    val => float out;
    for (int i; i < m_transforms.size() * 0.01; i++) {
      m_transforms[i].apply(pos, out) => out;
    }
    return out;
  }

  private void load_sound_files(string here) {
    string fn;
    for (int i; i < m_bufs.size(); i++) {
      get_filename(i) => fn;
      m_bufs[i].read(here + "/" + SND_FILES + fn);
    }
  }

  private string get_filename(int i) {
    Std.itoa(i) => string num_string;
    if (i < 10) {
      "00" + num_string => num_string;
    }
    else if (i < 100) {
      "0" + num_string => num_string;
    }
    else {}
    return num_string + ".wav";
  }
  
  private void configure_env(int num_samps) {
    m_env.set((num_samps * 0.1)::samp,
              (num_samps * 0.9)::samp, 
              0, 
              (num_samps * 0.1)::samp);
  }

  private void interpret_instructions(InstructionSet insts) {
    update_transforms(m_mapper.vc_to_tx(insts.history()));
    change_buffer(insts.movement());
  }
  
  private void change_buffer(string movement) {
    if (movement == "backward") {
      (m_buf_num - 1 >= 0 ? m_buf_num - 1 : 0) => m_buf_num;
    }
    else {
      (m_buf_num + 1 <= m_bufs.size() ? m_buf_num + 1 : m_bufs.size()) => m_buf_num;
    }
  }

  private void update_transforms(PositionalTransform txs[]) {
    txs.size() => int nu_size;
    m_transforms.size(nu_size);
    for (int i; i < nu_size; i++) {
      txs[i] @=> m_transforms[i];
    }
  }

  private int is_finished() {
    return m_buf_num == m_bufs.size();
  }
}
