class IP {
  int port;
  string ip;
  
  fun static IP create(string ip, int port) {
    new IP @=> IP out;
    ip => out.ip;
    port => out.port;
    return out;
  }
}

public class ConnMapper {
  IP m_mappings[0]; // [[ip, port]]
  StringSet m_names;

  fun void init() {
    StringSet.create().
      add("agnes1").add("agnes2").
      add("ethel1").add("ethel2").
      add("vera1").add("vera2").
      add("local1").add("local2").
      add("local3").add("local4").
      add("local5").add("local6") @=> m_names;
    IP.create("192.168.1.10", 5000) @=> m_mappings["agnes1"];
    IP.create("192.168.1.10", 5001) @=> m_mappings["agnes2"];
    IP.create("192.168.1.20", 5000) @=> m_mappings["ethel1"];
    IP.create("192.168.1.20", 5001) @=> m_mappings["ethel2"];
    IP.create("192.168.1.30", 5000) @=> m_mappings["vera1"];
    IP.create("192.168.1.30", 5001) @=> m_mappings["vera2"];
    IP.create("localhost", 5000) @=> m_mappings["local1"];
    IP.create("localhost", 5001) @=> m_mappings["local2"];
    IP.create("localhost", 5002) @=> m_mappings["local3"];
    IP.create("localhost", 5003) @=> m_mappings["local4"];
    IP.create("localhost", 5004) @=> m_mappings["local5"];
    IP.create("localhost", 5005) @=> m_mappings["local6"];
  }

  fun string ip_for(string name) {
    return m_mappings[name].ip;
  }

  fun int port_for(string name) {
    return m_mappings[name].port;
  }

  fun int size() { return m_names.size(); }
  fun StringSet keys() { return m_names; }
}
