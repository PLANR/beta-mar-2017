public class Receiver {
  OscIn in;
  OscMsg msg;
  Message omsg;
  string m_name;

  fun void init(string name, ConnMapper map) { 
    name => m_name; 
    in.port(map.port_for(name));
    in.addAddress(m_name);
  }
  
  fun void listen(Receive re, Parse pe) {
    while (true) {
      listen_for_messages();
      pe.message(omsg);
      pe.broadcast();
      ms => now;
    }
  }

  private void listen_for_messages() {
    0 => int message_received;
    while (!message_received) {
      in => now;
      while (in.recv(msg)) {
        /* <<< m_name, "got:", msg.getString(0), msg.getString(1) >>>; */
        omsg.movement(msg.getString(0));
        omsg.history(msg.getString(1));
        1 => message_received;
      }
    }
  }
}
