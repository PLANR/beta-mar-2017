public class TxCount {
  PositionalTransform tx;
  int count;
  
  fun static TxCount create(int count, PositionalTransform tx) {
    new TxCount @=> TxCount out;
    tx @=> out.tx;
    count => out.count;
    return out;
  }
}

