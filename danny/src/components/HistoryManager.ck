public class HistoryManager {
  VectorClock.create() @=> VectorClock m_history;
  string m_name;
  
  fun HistoryManager init(string name) {
    name => m_name;
  }

  fun void listen(Update ue, Play pe) {
    while (true) {
      /* <<< m_name, "update loop", "" >>>; */
      ue => now;
      /* <<< m_name, "update do", "" >>>; */
      ue.history().merge(m_history).add_event(m_name) @=> m_history;
      pe.instructions(ue.instructions().set_history(m_history));
      pe.signal();
    }
  }
}
