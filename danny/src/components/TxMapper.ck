public class TxMapper {
  PositionalTransform m_mapping[0];
  ["agnes1", "agnes2",
    "ethel1", "ethel2",
    "vera1", "vera2",
    "local1", "local2",
    "local3", "local4",
    "local5", "local6"] @=> string m_names[];
  
  fun void init() {
    new WeakenEvens @=> m_mapping["agnes1"];
    new WeakenOdds @=> m_mapping["agnes2"];
    new Drop @=> m_mapping["ethel1"];
    new Identity @=> m_mapping["ethel2"];
    new Drop @=> m_mapping["vera1"];
    new WeakenEvens @=> m_mapping["vera2"];

    new Drop @=> m_mapping["local1"];
    new WeakenOdds @=> m_mapping["local2"];
    new Drop @=> m_mapping["local3"];
    new Identity @=> m_mapping["local4"];
    new WeakenEvens @=> m_mapping["local5"];
    new WeakenOdds @=> m_mapping["local6"];
  }

  fun PositionalTransform[] vc_to_tx(VectorClock vc) {
    vc.keys() @=> StringSet vc_keys;
    PositionalTransform out[0];
    for (int i; i < vc_keys.size(); i++) {
      vc.at(vc_keys.at(i)) => int count;
      for (int j; j < count; j++) {
        out << m_mapping[vc_keys.at(i)];
      }
    }
    return out;
  }
}
