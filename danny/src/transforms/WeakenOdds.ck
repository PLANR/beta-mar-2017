public class WeakenOdds extends PositionalTransform {
  0.000001 => float m_degradation;

  fun WeakenOdds init(float degrade) {
    degrade => m_degradation;
    return this;
  }

  fun float apply(int pos, float val) {
    if (pos % 2 == 0)
      return val;
    else
      return val == 0 ? val :
                (val > 0 ? val - m_degradation : 
                           val + m_degradation);
  }
}
