public class Drop extends PositionalTransform {
  fun float apply(int pos, float val) {
    if (Math.random2f(0.0, 1.0) >= 0.99999) {
      return 0.0;
    }
    return val;
  }
}
