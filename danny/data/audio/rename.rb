audio_fns = Dir.entries('.').select {|fn| fn.end_with? '.wav' }
audio_fns.lazy.map {|fn| /(\d+)(\.wav)$/.match(fn) }.
  map { |md| { id: md[1].rjust(3, '0'), ext: md[2] } }.
  map { |h| h[:id] + h[:ext] }.
  zip(audio_fns).
  each { |new, old | File.rename(File.join('.', old), File.join('.', new)) }

