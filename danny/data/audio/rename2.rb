audio_fns = Dir.entries('.').select {|fn| fn.end_with? '.wav' }
audio_fns.lazy.with_index.
  each { |old, new| File.rename(File.join('.', old), File.join('.', new.to_s.rjust(3, '0'))) }
