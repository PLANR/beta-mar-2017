Dir.entries('.').lazy.reject { |fn| fn.start_with? '.' }.
  each { |fn| File.rename(File.join('.', fn), File.join('.', fn + '.wav')) }
