re = /([A-Z]\w+ begat[^;:\.]*)[;:\.]/

bib = File.open('./bible.txt', 'r') do |f|
  f.readlines.join.scan(re).lazy.
    map(&:first).
    map { |s| s.gsub(/\d:?/, '') }.
    map { |s| s.gsub(/[\n\r]+/, ' ') }
end

File.open('./text.txt', 'w') do |f|
  bib.take_while { |s| !s.include? 'circumcised' }.
    each do |l|
    f << l
    f << "\n"
  end
end
