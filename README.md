## we have 8 speakers: 6 medium 2 small
Code for computer music concert 11 March, 2017 @ BetaLevel.
Collaborators:
- Eric Heep [ericheep.com](http://ericheep.com)
- Danny Clarke [clarkenciel.com](https://clarkenciel.com)
- Michael Matthew

------------------------------------
| Computer | IP Addresses  | Being |
|:--------:|:-------------:|:-----:|
| Agnes | 192.168.1.10  | Raspberry Pi |
| Ethel | 192.169.1.20  | Raspberry Pi |
| Vera  | 192.168.1.30  | Raspberry Pi |
| Mike     | 192.168.1.100 | Human |
| Eric     | 192.168.1.xxx | Human |
| Danny    | 192.168.1.xxx | Human |
------------------------------------
